// ==UserScript==
// @name         mangadex-linear-key-scroll
// @namespace    https://mangadex-userscripts.blackandwhite.world/
// @version      0.3
// @description  enables linear scrolling in mangadex using arrow keys, wasd keyset, and hjkl (vim-alike).
// @author       zeen3
// @match        https://mangadex.org/*
// @match        https://mangadex.cc/*
// @match        https://e-hentai.org/*
// @match        https://exhentai.org/*
// @match        https://mangatoon.mobi/*
// @run-at       document-start
// @grant        none
// ==/UserScript==

'use strict';
const {min, max, abs, log2, round} = Math;

/*
#[repr(u8)]
#[derive(Clone, Copy, Debug)]
enum Action {
  PosY = 1,
  PosX = 2,
  NegY = 3,
  NegX = 4,
  Next = 5,
  Prev = 6,
}
struct Actions {
  data: HashMap<smartstring::alias::String, [u8; 16]>,
}
// potentially faster, due to the short lengths involved:
struct ActionsFast {
  keys: Vec<KeyCode>,
  data: Vec<[Action; 16]>
}
// simpler
type ActionsFastVecMap = vector_map::VecMap<KeyCode, [Action; 16]>;

// In a distributed (serviceworker-driven) application, could be something
// a little more messy, with raw pointers to a shared memory array and a set of
// raw vectors. ie: (SharedArena<[u8; 0x0400]>, SharedArena<[u8; 0x4000]>) for
// a set of (keys, values) would be more than enough, but you'd have to prepare
// the key mappings ahead of time. How this is set p allows actions to be updated
// from a clone from another thread, and relies a lot on short strings.
// Simply put, not worth rewriting this in rust, as Objects are necessary to perform
// the String -> Int mapping anyway, and `which` is preferred to be avoided.
//
// In the distributed case, serviceworker would `postMessage` an {actions: {...actions}} value,
// though preferably with a shared buffer rather than cloning each buffer.
// Could also interlace it to be {keys: string[], data: ArrayBuffer}, since ArrayBuffers
// slice/copy extremely quickly and can be transferred from thread to thread.
// For example:
//
// ```js
// const buf = Symbol("actions buffer");
// let {keys, buf} = {"keys": Object.keys(actions), "buf": actions[buf]}
// clients.getAll({"type": "window"})
// .then(clients => clients.forEach(client => {
//   let buf = buf.slice();
//   client.postMessage({"keys": keys, "buf": buf}, [buf])
// }));
//
*/
const act_buf = Symbol("actions buffer");
const actions = {};
{
  const actions_defaults = {
    // wasd
    "KeyS": [1],
    "KeyD": [2],
    "KeyW": [3],
    "KeyA": [4],
    // hjkl
    "KeyJ": [1],
    "KeyL": [2],
    "KeyK": [3],
    "KeyH": [4],
    // udlr
    "ArrowDown": [1],
    "ArrowRight": [2],
    "ArrowUp": [3],
    "ArrowLeft": [4],
    // rl
    "BracketRight": [5],
    "BracketLeft": [6],
  };
  const keys = Object.keys(actions_defaults);
  const k = new ArrayBuffer(keys.length * 16);
  actions[act_buf] = k;
  for (let [i, code] of keys.entries()) (actions[code] = new Int8Array(k, i << 4, 16)).set(actions_defaults[code]);
}
/*
struct Settings {
  // x,y multipliers
  multipliers: (f32, f32),
  speedup: Speedup,
}
struct Speedup {
  // after how long (in ms) should it begin linear acceleration to max.
  post: f64,
  // how long it should take to accelerate from 1 to max
  duration: f64,
  // how fast it should go at max
  max: f64,
}
*/
// due to scale, this is 1000px * [x, y] per second.
const dtxy = {x: 0.84, y: 0.94, t: {post: 600.0, duration: 2000, max: 1.2}};
Object.seal(dtxy.t);
Object.seal(dtxy);
/*
#[derive(Copy, Clone)]
#[repr(C, align(4))]
struct DirsVal {
  py: u8,
  px: u8,
  ny: u8,
  nx: u8,
}
enum Dir { Down, Right, Up, Left }
impl DirsVal {
  fn get_dir(&mut self, d: Dir) -> &mut u8 {
    use Dir::*;
    match d {
      Down => &mut self.py,
      Right => &mut self.px,
      Up => &mut self.ny,
      Left => &mut self.nx,
    }
  }
  fn vec2(&self) -> (i8, i8) {
    // if you hit 128 keys down... in one direction... you're probably fucked anyway.
    #[cfg(feature = "keys128")]
    let i = |v| if v & 0x80 == 0 { v as i8 } else { 0x7f };
    #[cfg(not(feature = "keys128"))]
    let i = |v| v as i8;

    let &Self { py, px, ny, nx } = self;
    // can be done in simd but it's 2 bytes on 2 bytes... probably already in registers.
    (i(py) - i(ny), i(px) - i(nx))
  }
}
impl AddAssign<Dir> for DirsVal {
  fn add_assign(&mut self, d: Dir) {
    let add = self.get_dir(d);
    *add = (*add).saturating_add(1);
  }
}
impl SubAssign<Dir> for DirsVal {
  fn sub_assign(&mut self, d: Dir) {
    let sub = self.get_dir(d);
    *sub = (*sub).saturating_sub(1);
  }
}
// effectively a transmute between i32 and [u8; 4], with saturating u8 values.
union Dirs {
  _fast: i32,
  dirs: DirsVal,
}
impl Dirs {
  pub fn vec2(self) -> (i8, i8) {
    unsafe {
      match self {
        // effectively a nonzero check
        Dirs { _fast: 0 } => None,
        Dirs { dirs } => dirs.vec2().into(),
      }
    }
  }
}
*/
// shared, fast buffer
const _d = new Int32Array(1);
/** directions anticlockwise starting from positive Y (down). */
const dirs = new Uint8ClampedArray(_d.buffer, _d.byteOffset, 4);
/*
//  massive thing that can go fast
struct Scroller {
  settings: Settings,
  dirs: Dirs,
  t0: f64,
  t1: f64,
  f_average: f64,
  anim: Option<AnimationFrameRequest>,
  scrolled_frames: i32,
}
impl Scroller {
  fn new(settings: Settings) {
    Self {
      settings,
      dirs: Dirs { _fast: 0 },
      t0: f64::EPSILON,
      t1: f64::EPSILON,
      t_average: 30.0,
      anim: None,
      scrolled_frames: i32,
    }
  }
  fn dpr() -> f64 {
    // this is a value that can change while on the window and even between multiple
    // windows/tabs. AKA fuck.
    WINDOW.get("devicePixelRatio").unwrap()
  }
  fn scroller(&mut self, tn: f64) {
    self.anim = None;
    match self.dirs.vec2() {
      None => {
        if self.scrolled_frames != 0 {
          self.t_average = (self.t0 - self.t1) / self.scrolled_frames;
          self.scrolled_frames = 0;
        }
      },
      Some((dy, dx)) => {
        self.scrolled_frames += 1;
        let mut dt = (tn - t0).abs().min(self.t_average * 3.0) / Self::dpr();
        let mut dmul = tn.sub(self.t1).sub(self.settings.speedup.post);
        if dmul > 0.0 {
          dmul /= self.settings.speedup.duration;
          dmul = dm.clamp(0.0, self.settings.speedup.max) + 1.0;
          dt *= dmul;
        }
        let scrolling_el = WINDOW.get_scrolling_element().unwrap();
        let muls = self.settings.multipliers;
        scrolling_el.scroll_by(dt * muls.0, dt * muls.1);
        self.t0 = tn;
        self.raf(None);
      },
    };
  }
  fn raf<T0: Into<Option<f64>>>(&mut self, t0: T0) {
    self.anim_raf();
    if let Some(t0) = t0.into() {
      self.t1 = t0;
      self.t0 = t0;
    }
  }
  fn anim_raf(&mut self) {
    self.anim.get_or_insert_with(|| Some(WINDOW.request_animation_frame(|t| self.scroller(t))));
  }
}
*/
let
// origin time
t0 = Number.EPSILON,
    // origin scroll time
    t_1 = t0,
    // frame average
    t_avg = 30,
    // animation frame value
    raf = null,
    frames_scroll = 0|0;
const measure_scroll = means => {
  // slightly anon data
  frames_scroll && (t_avg = (t0 - t_1) / frames_scroll)// && performance.measure(`scroll-action-end=${means}`, 'scroll-action-start');
  frames_scroll &= 0;
};
const scroller = T => {
  if (_d[0] !== 0) {
    // measure first frame action (or keep going)
    frames_scroll++// || performance.measure('scroll-action-frame=0', 'scroll-action-start');
    // time difference maxing at 3 frames (average time). First time means we could jump up to (90*[xmul, ymul])/dpr pixels immediately.
    let dt = min(abs(T - t0), t_avg * 3) / devicePixelRatio;
    // time since first scroll
    let dm = (T - t_1) - dtxy.t.post;
    if (dm > 0) {
      // accelerate linearly after `post` time has passed
      dm /= dtxy.t.duration;
      // accelerate from at least 1.0 to `max`.
      dm = max(1, min(dtxy.t.max, 1 + dm));
      dt *= dm;
    }
    const dy = dirs[0] - dirs[2];
    const dx = dirs[1] - dirs[3];
    // // log distance
    // console.debug('scroll %i,%i', dx, dy);
    const sc = document.fullscreenElement || document.scrollingElement || document.body;
    // scrolling by time and space
    sc.scrollBy(dt * dtxy.x * +dx, dt * dtxy.y * +dy);
    // set our origin time to this frame
    t0 = T;
    // run the scroller next frame (garbage collection time notwithstanding)
    raf = requestAnimationFrame(scroller);
  } else {
    // this is almost definitely an idle frame, so we can take a lot more time than usual (100+ ms).
    raf = null;
    // measure time in the case we need the analytical data
    measure_scroll('no-dxy');
  }
};
/*
impl Scroller {
  fn dir_action(&mut self, evt: Event, dir: Dir) {
    self.dirs += dir;
    self.raf(evt.time_stamp());
  }
  fn scroll_finish(&mut self, _evt: Event, dir: Dir) {
    self.dirs -= dir;
  }
  fn cancel(&mut self) {
    if let Some(raf) = self.anim.take() {
      WINDOW.cancel_animation_frame(raf);
    }
    self.dirs = Dirs { _fast: 0 };
  }
}
*/
const scr = (e, d) => {
  ++dirs[d];
  raf || (
    // raf on scroller for next frame (always async)
    raf = requestAnimationFrame(scroller),
    // update origin time to time event was fired
    t0 = t_1 = +e.timeStamp
  );
};
const dec = d => { --dirs[d]; };
const can = () => {
  // we don't want frames to continue beyond this point
  cancelAnimationFrame(raf);
  // we don't need the scroller
  _d[0] = 0|0;
  // set the animation frame to None
  raf = null;
  // measure how long we scrolled for
  measure_scroll('cancel');
};
document.addEventListener('visibilitychange', can);
const performAction = (action, e) => {
  action = action | 0;
  switch (action|0) {
    case 1: case 2: case 3: case 4:
      // if we're not scrolling we add a mark
      //_d[0] || performance.mark('scroll-action-start');
      if (e.repeat) e.stopImmediatePropagation();
      // scroll
      else scr(e, action - 1);
      if (action & 1) {
        // if we're scrolling vertically, we can always go forward and back (on MD anyways)
        e.stopImmediatePropagation();
      } else if (scrollX > Number.EPSILON && (scrollX + innerWidth) < document.body.scrollWidth) {
        // if we're not on the horizontal edges, we can go forward and back
        e.stopImmediatePropagation();
      } else if (
        // if we're on a vertical edge but we're scrolling towards the centre,
        // we have to go back for user experience.
        document.body.scrollWidth > innerWidth && (
          // increasing scrollX is fine if we're not on the right edge
          (scrollX <= Number.EPSILON && action === 2)
          // decreasing scrollX is fine if we're not on the left edge
          || (scrollX > Number.EPSILON && (scrollX + innerWidth) >= document.body.scrollWidth && action === 4)
        )
      ) e.stopImmediatePropagation();
      // we can't scroll further at this point, so on each repeat we saturate down
      else dec(action - 1);
      e.preventDefault();

      break;

    case 5:
      // mangatoon was a dick, so I added ] as a Next selector
      location.host === 'mangatoon.mobi' && document.querySelector('#read-next-link-wrapper').click();
      break;
    case 6:
      // mangatoon was a dick, so I added ] as a Prev selector
      location.host === 'mangatoon.mobi' && document.querySelector('.nav-bar-bottom > a[href]:last-of-type').click();
      break;
  }
};
const performedAction = (action, e) => {
  action = action | 0;
  switch (action) {
    case 1: case 2: case 3: case 4:
      // 1..=4, decrement for these
      dec(action - 1);
      break;
  }
};
{
  const ctrlkeys = 'OS,Control,Ctrl,Shift,Meta,Alt'.split(',').reduce((s,v) => s.add(v + 'Left').add(v + 'Right'), new Set);
  const keyperformer = fn => e => {
    // Input elements disable the dialer functions
    if ('value' in e.target) can();
    // set up actions and modifiers
    else if (e.code in actions) {
      const mod = 0 |
            (e.shiftKey << 0) |
            (e.metaKey << 1) |
            (e.ctrlKey << 2) |
            (e.altKey << 3);
      const action = actions[e.code][mod];
      fn(action|0, e);
    }
    // control keys (Ctrl, Shift, Meta/Win, Alt) disable the dialer functions
    else if (ctrlkeys.has(e.code)) can();
    // catch-all, should help with dumb shit like randomly pressing 0/Numpad0/etc
    else console.log(e);
  };
  document.addEventListener('keydown', keyperformer(performAction));
  document.addEventListener('keyup', keyperformer(performedAction));
}
